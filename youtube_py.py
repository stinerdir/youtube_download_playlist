#!/usr/bin/env python3
import youtube_dl as tube
import os
import time
import pafy
import pytube
number_choice = input("item selection: \n[1]-download video \n[2]-download Playlist \n\n[#]>: ")


def singel_down():
    link = input('\ncopy and paste your YouTube URL here: \n[#]>: ')

    yt = pytube.YouTube(link)
    views_vid = yt.views
    disc = yt.description
    rating_vid = yt.rating
    video_singel = pafy.new(link)
    print("\n\n", "video length: ", video_singel.length)

    print("\n\nvideo views: ", str(views_vid), "\nvideo description:",
          disc, "\nvideo rating: ", rating_vid, "\n")
    filename_youtube = input("\nPlease Enter name file:  \n[#]>: ")
    stream = yt.streams.first()
    print("stating download ...")
    finished = stream.download(filename=filename_youtube)
    print('\nDownload Done!')


def playlist_tube():

    links = input("copy and paste your YouTube URL Playlist here: \n[#]>: ")
    # views_vid = links.views
    # disc = links.description
    # rating_vid = links.rating
    # print("\n\nvideo views: ",str(views_vid),"\nvideo description:",disc,"\nvideo rating: ",rating_vid,"\n")

    playlist = pytube.Playlist(links)
    playlist.populate_video_urls()
    print("List size: ", len(playlist.video_urls))
    print("strting download ...")
    playlist.download_all()
    print("download done!")


if number_choice == "1":
    singel_down()
if number_choice == "2":
    playlist_tube()
